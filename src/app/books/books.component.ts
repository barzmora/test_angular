import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';

import { BooksService } from '../books.service';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books = [];
  task: string;
  task1:string;
  show = 'no button';

  constructor(private db:AngularFireDatabase,private authService:AuthService,private booksService:BooksService) { }

  ngOnInit() {
      this.authService.user.subscribe(
        user => {
          this.db.list('/users/'+user.uid+'/books').snapshotChanges().subscribe( // tell me for all the data changes on the things below
            books => { // the array he took from the FireBase
              this.books = [];
              books.forEach(
                book => {
                  let task = book.payload.toJSON();
                  task['key'] = book.key;
                  this.books.push(task);
                }
              )
            }
          )
        }
      )
  }
  showText(text) {
    this.show = text;
}
  addBook() {
    this.booksService.addBooks(this.task);
    this.task = '';
  }
}
