import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {AuthService} from '../auth.service';
import { BooksService } from '../books.service';
import {Router} from "@angular/router";

@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  @Input() data:any;
  @Output() myButtonClicked = new EventEmitter<any>();

  showButton = false;
  showEditField = false;
  bookName: string;
  authorName: string;
  key;

  constructor(private authService:AuthService,private router:Router,private booksService:BooksService) { }

  ngOnInit() {
    this.bookName = this.data.bookName;
    this.authorName = this.data.authorName;
    this.key = this.data.key;
  }

  onClick() {
    this.myButtonClicked.emit(this.bookName);
  }

  buttonOn() {
    this.showButton = true;
  }

  buttonOff() {
    this.showButton = false;
  }

  delete() {
    this.booksService.deleteBooks(this.key);
  }

 
  
  save() {
    this.booksService.updateBooks(this.key, this.bookName,this.authorName);
    this.showEditField = false;
  }

 

}
