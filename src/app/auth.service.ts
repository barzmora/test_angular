import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';
import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<firebase.User>;

  signUp(email:string, password:string){
    return this.firebaseAuth
     .auth
     .createUserWithEmailAndPassword(email,password);
   }
   
   updateProfile(user,name:string){
     user.updateProfile({displayName:name,photoURL:''});
   }
 
   login(email:string, password:string){
     return this.firebaseAuth
     .auth.signInWithEmailAndPassword(email,password)
   }
 
   logOut(){
     return this.firebaseAuth.auth.signOut();
   }
 
   addUser(user, name: string) {
    let uid = user.uid;
    let ref = this.db.database.ref('/'); // the main endpoint of the db
    ref.child('users').child(uid).push({'nickname':name}); // if the child doesn't exist - he creates it
  }
 
  
 

  constructor(private firebaseAuth: AngularFireAuth,
    private db:AngularFireDatabase) {
    this.user = firebaseAuth.authState;
   }
}
