import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import {environment} from '../environments/environment';
import { Routes, RouterModule } from '@angular/router';
//material design imports
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';

//fire base imports
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { NavComponent } from './nav/nav.component';
import { BooksComponent } from './books/books.component';
import { BookComponent } from './book/book.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';





@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    BookComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
   AngularFireDatabaseModule,
   AngularFireAuthModule,
   MatCardModule,
   MatInputModule,
   BrowserAnimationsModule,
   MatButtonModule,
   CommonModule,
   FormsModule,
   RouterModule.forRoot([
    { path:'', component: BooksComponent },
    { path:'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path:'**', component: BooksComponent}
  ])
  


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
