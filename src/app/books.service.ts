import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private authService:AuthService, private db:AngularFireDatabase) { }
  
  addBooks(book: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/books').push({'bookName': book,'authorName':book});
      }
    )
  }

  deleteBooks(key: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/books').remove(key);
      }
    )
  }

  updateBooks(key, bookName,authorName) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/books').update(key,{'bookName':bookName,'authorName':authorName});
      }
    )
  }
}
